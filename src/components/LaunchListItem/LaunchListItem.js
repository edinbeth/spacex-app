import React from 'react';
import './LaunchListItem.scss';

const LaunchListItem = (props) => {
  return (
	<li key={ props.launch.index }>
		<span className="flight-number">#{ props.launch.flight_number }</span>
		<span className="mission-name">{ props.launch.mission_name }</span>
		<span className="launch-date">{ props.date }</span>
		<span className="rocket-name">{ props.launch.rocket.rocket_name }</span>
	</li>
  );
};

export default LaunchListItem;