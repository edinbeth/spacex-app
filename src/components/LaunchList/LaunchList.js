import React from 'react';
import moment from 'moment';
import logo from '../../assets/spacex-logo.png';
import './LaunchList.scss';
import LaunchListItem from '../LaunchListItem/LaunchListItem';

class LaunchList extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      launches: [],
      baseLaunches: [],
      orderAscending: true
    };
  }

  componentDidMount(){
    this.fetchData();
  }

  fetchData(){
		var data;

		fetch('https://api.spacexdata.com/v3/launches')
			.then(res => res.json())
			.then(response => data = response)
			.then(() => this.setState({ launches: data, baseLaunches: data }));
  }

  sortData(){
  	var sortedData = this.state.launches.sort(function(a, b){
  		const firstDate = new Date(a.launch_date_local); 
  		const secondDate = new Date(b.launch_date_local); 

  		var result;
  		if (this.state.orderAscending){
  			result = firstDate < secondDate ? 1 : -1;
  		} else {
  			result = firstDate > secondDate ? 1 : -1;
  		}

  		return result;
  	}.bind(this));

		this.setState({ launches: sortedData });
		this.setState({ orderAscending: !this.state.orderAscending });
		document.getElementById('sortButton').innerText = `Sort ${this.state.orderAscending ? "Ascending" : "Descending"}`;
  }

  filterByYear(event){
  	const year = event.target.value;

  	if(year === 'Filter by Year'){
  		this.setState({ launches: this.state.baseLaunches });
  		return;
  	}

  	var filteredLaunches = this.state.baseLaunches.filter(launch => launch.launch_year === year);
  	this.setState({ launches: filteredLaunches});
  }

  render(){
	  var launchlist = this.state.launches.map((launch, index) => {
	  	var date = moment(launch.launch_date_local).format('Do MMM YYYY');
    	return <LaunchListItem key={ index } launch={ launch } date={ date }/> });
  
    var years = this.state.baseLaunches.map(launch => launch.launch_year);
    var uniqueYears = [...new Set(years)];
    var yearOptions = uniqueYears.map((year, index) => {
    	return <option key={ index }>{ year }</option>
    });

    return (
      <div id="wrapper">
      	<header>
      		<img src={logo} alt="SpaceX Logo" id="headerLogo"/><span className="logo-label">LAUNCHES</span>
      		<button className="reload" onClick={ this.fetchData.bind(this) }>Reload Data</button>
      	</header>
      	<main>
      		<div id="controls">
	        	<select id="filterSelect" onChange={ this.filterByYear.bind(this) }>
	        		<option>Filter by Year</option>
	        		{ yearOptions }
	        	</select>
	        	<button id="sortButton" onClick={ this.sortData.bind(this) }>Sort Descending</button>
	        </div>
	        <ul>
	        	{ launchlist }
	        </ul>
        </main>
      </div>
    );
  }
}

export default LaunchList;