import React from 'react';
import ReactDOM from 'react-dom';
import LaunchList from './LaunchList';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<LaunchList />, div);
  ReactDOM.unmountComponentAtNode(div);
});
